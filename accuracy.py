dir = "."

with open("%s/result4.txt" % dir, "r") as f:
    sents = [line.strip() for line in f.readlines() if line.strip()]

total = len(sents)
print(total)

ok = {}
counts = {}

count = 0
for sent in sents:
    words = sent.split()
    counts[words[-2]]= counts.get(words[-2], 0) + 1
    if words[-1] == words[-2]:
        count += 1
        ok[words[-2]]= ok.get(words[-2], 0) + 1


print(counts)
print(ok)
for k,v in counts.items():
	print("Accuracy %s: %.4f" %(k,ok[k]/v))

print("Accuracy total: %.4f" %(count/total))

# 0.9706
